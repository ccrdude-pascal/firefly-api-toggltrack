{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Test form to display Toggl Track API data.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2023 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2023-08-04  pk  ---  Added heade.
// *****************************************************************************
   )
}

unit FireflyTogglTrackTest.FormMain;

{$IFDEF FPC}
{$mode Delphi}{$H+}
{$ENDIF FPC}
interface

uses
   Classes,
   SysUtils,
   Forms,
   Controls,
   Graphics,
   Dialogs,
   ComCtrls,
   StdCtrls,
   Firefly.TogglTrack.V9,
   OVM.ComboBox,
   OVM.ListView;

type

   { TFormFireflyTogglTrackTest }

   TFormFireflyTogglTrackTest = class(TForm)
      bnStartTimer: TButton;
      bnRefreshCurrentTimeEntry: TButton;
      bnStopTimer: TButton;
      cbWorkspaces: TComboBox;
      cbProjects: TComboBox;
      labelWorkspace: TLabel;
      labelProject: TLabel;
      lvCurrentTimeEntry: TListView;
      lvClients: TListView;
      lvTimeEntries: TListView;
      lvWorkspaces: TListView;
      lvTags: TListView;
      lvProjects: TListView;
      PageControl1: TPageControl;
      tabClients: TTabSheet;
      tabActive: TTabSheet;
      tabTimeEntries: TTabSheet;
      tabWorkspaces: TTabSheet;
      tabTags: TTabSheet;
      tabProjects: TTabSheet;
      procedure bnRefreshCurrentTimeEntryClick({%H-}Sender: TObject);
      procedure bnStartTimerClick({%H-}Sender: TObject);
      procedure bnStopTimerClick(Sender: TObject);
      procedure FormCreate({%H-}Sender: TObject);
      procedure FormDestroy({%H-}Sender: TObject);
      procedure FormShow({%H-}Sender: TObject);
   private
      FDemoUsername: string;
      FDemoPassword: string;
      FCurrentTimeEntry: TTogglTrackTimeCurrentEntry;
      FAPI: TTogglTrackAPIv9;
      procedure LoadSettings;
      procedure SaveSettings;
      function CheckToken: boolean;
   public
   end;

var
   FormFireflyTogglTrackTest: TFormFireflyTogglTrackTest;

implementation

uses
   Registry;

   {$R *.lfm}

procedure TFormFireflyTogglTrackTest.FormShow(Sender: TObject);
begin
   LoadSettings;
   if not CheckToken then begin
      Exit;
   end;
   FAPI.RefreshMe;
   lvTags.DisplayItems<TTogglTrackTag>(FAPI.Tags);
   lvWorkspaces.DisplayItems<TTogglTrackWorkspace>(FAPI.Workspaces);
   cbWorkspaces.DisplayItems<TTogglTrackWorkspace>(FAPI.Workspaces);
   if cbWorkspaces.Items.Count > 0 then begin
      cbWorkspaces.ItemIndex := 0;
   end;
   lvProjects.DisplayItems<TTogglTrackProject>(FAPI.Projects);
   cbProjects.DisplayItems<TTogglTrackProject>(FAPI.Projects);
   if cbProjects.Items.Count > 0 then begin
      cbProjects.ItemIndex := 0;
   end;
   lvClients.DisplayItems<TTogglTrackClient>(FAPI.Clients);
   lvTimeEntries.DisplayItems<TTogglTrackTimeEntry>(FAPI.TimeEntries);
end;

procedure TFormFireflyTogglTrackTest.FormCreate(Sender: TObject);
begin
   FAPI := TTogglTrackAPIv9.Create;
   FAPI.ProxyHost := '127.0.0.1';
   FAPI.ProxyPort := 8888;
   FCurrentTimeEntry := TTogglTrackTimeCurrentEntry.Create;
   FCurrentTimeEntry.ID := -1;
end;

procedure TFormFireflyTogglTrackTest.bnStartTimerClick(Sender: TObject);
var
   iWorkspace: int64;
   iProject: int64;
begin
   iWorkspace := cbWorkspaces.GetSelectedKey(0);
   if iWorkspace < 0 then begin
      MessageDlg('Mo workspace selected.', mtError, [mbOK], 0);
      Exit;
   end;
   iProject := cbProjects.GetSelectedKey(0);
   if iProject < 0 then begin
      MessageDlg('Mo workspace selected.', mtError, [mbOK], 0);
      Exit;
   end;
   FAPI.StartTimer(FCurrentTimeEntry, Now, iWorkspace, 'api test', iProject);
   lvCurrentTimeEntry.DisplayProperties(FCurrentTimeEntry);
end;

procedure TFormFireflyTogglTrackTest.bnStopTimerClick(Sender: TObject);
begin
   if FCurrentTimeEntry.ID < 0 then begin
      Exit;
   end;
   FAPI.StopTimer(FCurrentTimeEntry.WorkspaceID, FCurrentTimeEntry.ID);
end;

procedure TFormFireflyTogglTrackTest.bnRefreshCurrentTimeEntryClick(Sender: TObject);
begin
   lvCurrentTimeEntry.Items.BeginUpdate;
   try
      if FAPI.GetTimer(FCurrentTimeEntry) then begin
         if FCurrentTimeEntry.ID > -1 then begin
            lvCurrentTimeEntry.DisplayProperties(FCurrentTimeEntry);
         end else begin
            lvCurrentTimeEntry.Items.Clear;
         end;
      end else begin
         lvCurrentTimeEntry.Items.Clear;
      end;
   finally
      lvCurrentTimeEntry.Items.EndUpdate;
   end;
end;

procedure TFormFireflyTogglTrackTest.FormDestroy(Sender: TObject);
begin
   FCurrentTimeEntry.Free;
   FAPI.Free;
end;

procedure TFormFireflyTogglTrackTest.LoadSettings;
var
   reg: TRegistry;
begin
   reg := TRegistry.Create;
   try
      reg.RootKey := HKEY_CURRENT_USER;
      if reg.OpenKey('\Software\Firefly\TogglTrack\', True) then begin
         try
            if reg.ValueExists('DemoUsername') then begin
               Self.FDemoUsername := reg.ReadString('DemoUsername');
            end;
            if reg.ValueExists('DemoPassword') then begin
               Self.FDemoPassword := reg.ReadString('DemoPassword');
            end;
         finally
            reg.CloseKey;
         end;
      end;
      if reg.OpenKey('\Software\Firefly\TogglTrack\', True) then begin
         try
            if reg.ValueExists('DemoUsername') then begin
               Self.FDemoUsername := reg.ReadString('DemoUsername');
            end;
            if reg.ValueExists('DemoPassword') then begin
               Self.FDemoPassword := reg.ReadString('DemoPassword');
            end;
         finally
            reg.CloseKey;
         end;
      end;
   finally
      reg.Free;
   end;
end;

procedure TFormFireflyTogglTrackTest.SaveSettings;
var
   reg: TRegistry;
begin
   reg := TRegistry.Create;
   try
      reg.RootKey := HKEY_CURRENT_USER;
      if reg.OpenKey('\Software\Firefly\TogglTrack\', True) then begin
         try
            reg.WriteString('DemoUsername', Self.FDemoUsername);
            reg.WriteString('DemoPassword', Self.FDemoPassword);
         finally
            reg.CloseKey;
         end;
      end;
   finally
      reg.Free;
   end;
end;

function TFormFireflyTogglTrackTest.CheckToken: boolean;
begin
   Result := False;
   if Length(Self.FDemoUsername) = 0 then begin
      if not InputQuery('Toggl Track Login', 'Please enter username:', FDemoUsername) then begin
         Exit;
      end;
      SaveSettings;
   end;
   if Length(Self.FDemoPassword) = 0 then begin
      if not InputQuery('Toggl Track Login', 'Please enter password:', FDemoPassword) then begin
         Exit;
      end;
      SaveSettings;
   end;
   if Length(Self.FDemoUsername) = 0 then begin
      Exit;
   end;
   if Length(Self.FDemoPassword) = 0 then begin
      Exit;
   end;
   FAPI.AuthMethod := ttamLogin;
   FAPI.LoginUsername := FDemoUsername;
   FAPI.LoginPassword := FDemoPassword;
   Result := True;
end;

end.
