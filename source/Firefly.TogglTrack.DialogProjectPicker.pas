unit Firefly.TogglTrack.DialogProjectPicker;

{$mode Delphi}{$H+}

interface

uses
   Classes,
   SysUtils,
   Forms,
   Controls,
   Graphics,
   Dialogs,
   ButtonPanel,
   ComCtrls,
   StdCtrls,
   OVM.ListView,
   OVM.ComboBox,
   Firefly.TogglTrack.V9;

type

   { TFormDialogTogglTrackProjectPicker }

   TFormDialogTogglTrackProjectPicker = class(TForm)
      ButtonPanel1: TButtonPanel;
      cbWorkspaces: TComboBox;
      lvProjects: TListView;
      procedure lvProjectsDblClick(Sender: TObject);
      procedure lvProjectsSelectItem(Sender: TObject; Item: TListItem; Selected: boolean);
   private
      FSelected: TTogglTrackProject;
   public
      function Execute(const AnAPI: TTogglTrackAPIv9; out AProject: TTogglTrackProject): boolean;
   end;

function TogglTrackProjectPicker(const AnAPI: TTogglTrackAPIv9; out AProject: TTogglTrackProject): boolean;

implementation

function TogglTrackProjectPicker(const AnAPI: TTogglTrackAPIv9; out AProject: TTogglTrackProject): boolean;
var
   form: TFormDialogTogglTrackProjectPicker;
begin
   form := TFormDialogTogglTrackProjectPicker.Create(nil);
   try
      Result := form.Execute(AnAPI, AProject);
   finally
      form.Free;
   end;
end;

{$R *.lfm}

{ TFormDialogTogglTrackProjectPicker }

procedure TFormDialogTogglTrackProjectPicker.lvProjectsSelectItem(Sender: TObject; Item: TListItem; Selected: boolean);
begin
   if (not Selected) or (not Assigned(Item)) then begin
      Exit;
   end;
   if not Assigned(Item.Data) then begin
      Exit;
   end;
   FSelected := TTogglTrackProject(Item.Data);
   ButtonPanel1.OKButton.Enabled := True;
end;

procedure TFormDialogTogglTrackProjectPicker.lvProjectsDblClick(Sender: TObject);
begin
   lvProjectsSelectItem(lvProjects, lvProjects.Selected, True);
   ButtonPanel1.OKButton.Click;
end;

function TFormDialogTogglTrackProjectPicker.Execute(const AnAPI: TTogglTrackAPIv9; out AProject: TTogglTrackProject): boolean;
begin
   cbWorkspaces.DisplayItems<TTogglTrackWorkspace>(AnAPI.Workspaces);
   if cbWorkspaces.Items.Count > 0 then begin
      cbWorkspaces.ItemIndex := 0;
   end;
   lvProjects.DisplayItems<TTogglTrackProject>(AnAPI.Projects);
   Result := (ShowModal = mrOk);
   AProject := FSelected;
end;

end.
