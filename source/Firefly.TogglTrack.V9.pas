{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Access to the Toggl Track API.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2023-2024 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2024-11-28  pk  10m  Added TDateTime
// 2023-08-04  pk  20m  Working authentication using login method
// 2023-08-03  pk  ---  First draft
// *****************************************************************************
   )
}

unit Firefly.TogglTrack.V9;

{$IFDEF FPC}
{$mode Delphi}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   Generics.Collections,
   Layers.Transport.Base,
   Layers.JSON.Base,
   OVM.Attributes,
   OVM.ListView.Attributes,
   OVM.ComboBox.Attributes;

type
   TTogglTrackAuthMethod = (ttamToken, ttamLogin);

   TTogglTrackAPIv9 = class;
   TTogglTrackTags = class;
   TTogglTrackWorkspaces = class;
   TTogglTrackProjects = class;
   TTogglTrackDataObject = class;

   {
     TTogglTrackDataObject is the parent class for all data items, defining
     the key methods common to all.
   }
   TTogglTrackDataObject = class abstract
   protected
      FOwner: TObject;
   public
      procedure LoadFromJSONObject(AnObject: TLayeredJSONObject); virtual; abstract;
      function DisplayText: string; virtual; abstract;
   end;

   {
     TTogglTrackDataObjects is the parent class all data containers should
     inherit from; it implements reading from a JSON array into the items.
   }
   TTogglTrackDataObjects<T: class> = class(TObjectList<T>)
   protected
      FOwner: TTogglTrackAPIv9;
      procedure ForEachObject({%H-}TheElementName: string; TheElement: TLayeredJSONObject; {%H-}AnUserData: pointer; var Continue: boolean); virtual;
   public
      procedure LoadFromJSONArray(AnArray: TLayeredJSONArray);
   end;

   {
     TTogglTrackWorkspace wraps up a workspace as seen in the Toggle Track API
     documentation.

     @link(TTogglTrackWorkspaces)
     @see(https://developers.track.toggl.com/docs/workspace Workspace)
   }
   TTogglTrackWorkspace = class(TTogglTrackDataObject)
   private
      FAdmin: boolean;
      FAPIToken: string;
      FAt: string;
      FBusinessWS: boolean;
      FID: int64;
      FName: string;
      FOnlyAdminsMayCreateProjects: boolean;
      FOnlyAdminsMayCreateTags: boolean;
      FOnlyAdminsSeeBillableRates: boolean;
      FOnlyAdminsSeeTeamDashboard: boolean;
      FOrganizationID: int64;
      FPremium: boolean;
      FProjectsBillableByDefault: boolean;
      FReportsCollapse: boolean;
   public
      procedure LoadFromJSONObject(AnObject: TLayeredJSONObject); override;
      function DisplayText: string; override;
      property OnlyAdminsMayCreateProjects: boolean read FOnlyAdminsMayCreateProjects;
      property OnlyAdminsMayCreateTags: boolean read FOnlyAdminsMayCreateTags;
      property OnlyAdminsSeeBillableRates: boolean read FOnlyAdminsSeeBillableRates;
      property OnlyAdminsSeeTeamDashboard: boolean read FOnlyAdminsSeeTeamDashboard;
      property ProjectsBillableByDefault: boolean read FProjectsBillableByDefault;
      property ReportsCollapse: boolean read FReportsCollapse;
   published
      [AComboBoxUniqueIDField()]
      property ID: int64 read FID;
      [AListViewColumnDetails('Organization', taRightJustify)]
      property OrganizationID: int64 read FOrganizationID;
      [AComboBoxDisplayText()]
      property Name: string read FName;
      [AOVMBooleanYesNo()]
      property Admin: boolean read FAdmin;
      [AOVMBooleanYesNo()]
      property Premium: boolean read FPremium;
      [AOVMBooleanYesNo()]
      property BusinessWS: boolean read FBusinessWS;
      property At: string read FAt;
      property APIToken: string read FAPIToken;
   end;

   {
     TTogglTrackWorkspaces manages a list of workspaces.

     @link(TTogglTrackWorkspace)
     @see(https://developers.track.toggl.com/docs/workspace Workspace)
   }
   TTogglTrackWorkspaces = class(TTogglTrackDataObjects<TTogglTrackWorkspace>);

   {
     TTogglTrackTag wraps up a tag as seen in the Toggle Track API
     documentation.

     @link(TTogglTrackTags)
     @see(https://developers.track.toggl.com/docs/tags Tags)
   }
   TTogglTrackTag = class(TTogglTrackDataObject)
   private
      FAt: string;
      FID: int64;
      FName: string;
      FWorkspaceID: int64;
   public
      procedure LoadFromJSONObject(AnObject: TLayeredJSONObject); override;
      function DisplayText: string; override;
   published
      property ID: int64 read FID;
      [AListViewColumnDetails('Workspace', taRightJustify)]
      property WorkspaceID: int64 read FWorkspaceID;
      property Name: string read FName;
      property At: string read FAt;
   end;

   {
     TTogglTrackTags manages a list of tracks.

     @link(TTogglTrackTag)
     @see(https://developers.track.toggl.com/docs/tags Tags)
   }
   TTogglTrackTags = class(TTogglTrackDataObjects<TTogglTrackTag>);

   {
     TTogglTrackProject wraps up a project as seen in the Toggle Track API
     documentation.

     @link(TTogglTrackProjects)
     @see(https://developers.track.toggl.com/docs/projects Projects)
   }
   TTogglTrackProject = class(TTogglTrackDataObject)
   private
      FActive: boolean;
      FAt: string;
      FClientID: int64;
      FCreatedAt: string;
      FID: int64;
      FIsPrivate: boolean;
      FName: string;
      FWorkspaceID: int64;
   public
      procedure LoadFromJSONObject(AnObject: TLayeredJSONObject); override;
      function DisplayText: string; override;
   published
      [AComboBoxUniqueIDField()]
      property ID: int64 read FID;
      [AComboBoxDisplayText()]
      property Name: string read FName;
      property WorkspaceID: int64 read FWorkspaceID;
      property ClientID: int64 read FClientID;
      [AOVMBooleanYesNo()]
      property Active: boolean read FActive;
      [AOVMBooleanYesNo()]
      property IsPrivate: boolean read FIsPrivate;
      property At: string read FAt;
      property CreatedAt: string read FCreatedAt;
   end;

   {
     TTogglTrackProjects manages a list of projects.

     @link(TTogglTrackProject)
     @see(https://developers.track.toggl.com/docs/projects Projects)
   }
   TTogglTrackProjects = class(TTogglTrackDataObjects<TTogglTrackProject>);

   {
     TTogglTrackClient wraps up a client as seen in the Toggle Track API
     documentation.

     @link(TTogglTrackClients)
   }
   TTogglTrackClient = class(TTogglTrackDataObject)
   private
      FArchived: boolean;
      FAt: string;
      FID: int64;
      FName: string;
      FWorkspaceID: int64;
   public
      procedure LoadFromJSONObject(AnObject: TLayeredJSONObject); override;
      function DisplayText: string; override;
   published
      property ID: int64 read FID;
      property Name: string read FName;
      property WorkspaceID: int64 read FWorkspaceID;
      [AOVMBooleanYesNo()]
      property Archived: boolean read FArchived;
      property At: string read FAt;
   end;

   {
     TTogglTrackClients manages a list of clients.

     @link(TTogglTrackClient)
   }
   TTogglTrackClients = class(TTogglTrackDataObjects<TTogglTrackClient>);

    {
     TTogglTrackTimeEntry wraps up a time entry as seen in the Toggle Track API
     documentation.

     @link(TTogglTrackTimeEntries)
   }
   TTogglTrackTimeEntry = class(TTogglTrackDataObject)
   private
      FBillable: boolean;
      FDescription: string;
      FDuration: int64;
      FID: int64;
      FStartText: string;
      FStopText: string;
      FTaskID: int64;
      FWorkspaceID: int64;
      function GetStart: TDateTime;
      function GetStop: TDateTime;
   public
      procedure LoadFromJSONObject(AnObject: TLayeredJSONObject); override;
      function DisplayText: string; override;
      property StartText: string read FStartText;
      property StopText: string read FStopText;
   published
      property ID: int64 read FID write FID;
      property WorkspaceID: int64 read FWorkspaceID;
      property TaskID: int64 read FTaskID;
      property Start: TDateTime read GetStart;
      property Stop: TDateTime read GetStop;
      property Billable: boolean read FBillable;
      property Duration: int64 read FDuration;
      property Description: string read FDescription;
   end;


   { TTogglTrackTimeCurrentEntry }

   TTogglTrackTimeCurrentEntry = class(TTogglTrackTimeEntry)
   private
      FAt: string;
      function GetCurrentDuration: TDateTime;
   public
      procedure LoadFromJSONObject(AnObject: TLayeredJSONObject); override;
   published
      property At: string read FAt;
      property CurrentDuration: TDateTime read GetCurrentDuration;
   end;

   {
     TTogglTrackTimeEntries manages a list of time entries.

     @link(TTogglTrackTimeEntry)
   }
   TTogglTrackTimeEntries = class(TTogglTrackDataObjects<TTogglTrackTimeEntry>);

   {
     TTogglTrackAPIv9 is the main interface for connecting to the
     Toggl Track API.
   }
   TTogglTrackAPIv9 = class
   private
      FAuthMethod: TTogglTrackAuthMethod;
      FClients: TTogglTrackClients;
      FLoginPassword: string;
      FLoginUsername: string;
      FProjects: TTogglTrackProjects;
      FProxyHost: string;
      FProxyPort: word;
      FTags: TTogglTrackTags;
      FTimeEntries: TTogglTrackTimeEntries;
      FToken: string;
      FUserAgent: string;
      FWorkspaces: TTogglTrackWorkspaces;
      function CreateTransport: TLayeredTransport;
      function ExecuteGET(AnURL: string; out AnObject: TLayeredJSONObject): boolean;
      function ExecutePOST(AnURL: string; AnInput: TLayeredJSONObject; out AnObject: TLayeredJSONObject): boolean;
      function ExecutePATCH(AnURL: string): boolean;
      function GetProjects: TTogglTrackProjects;
      function GetTimeEntries: TTogglTrackTimeEntries;
   public
      constructor Create; virtual;
      destructor Destroy; override;
      function HasToken: boolean;
      function RefreshMe: boolean;
      function StartTimer(const AnEntry: TTogglTrackTimeCurrentEntry; AStartTime: TDateTime; AWorkspaceID: integer; ADescription: string; AProjectID: integer = -1;
         ABillable: boolean = False): boolean;
      function GetTimer(const AnEntry: TTogglTrackTimeCurrentEntry): boolean;
      function StopTimer(AWorkspaceID, ATimeEntryID: int64): boolean;
      property AuthMethod: TTogglTrackAuthMethod read FAuthMethod write FAuthMethod;
      property Token: string read FToken write FToken;
      property LoginUsername: string read FLoginUsername write FLoginUsername;
      property LoginPassword: string read FLoginPassword write FLoginPassword;
      property Tags: TTogglTrackTags read FTags;
      property Workspaces: TTogglTrackWorkspaces read FWorkspaces;
      property Projects: TTogglTrackProjects read GetProjects;
      property Clients: TTogglTrackClients read FClients;
      property TimeEntries: TTogglTrackTimeEntries read GetTimeEntries;
      property ProxyHost: string read FProxyHost write FProxyHost;
      property ProxyPort: word read FProxyPort write FProxyPort;
      property UserAgent: string read FUserAgent write FUserAgent;
   end;

implementation

uses
   synacode,
   DateUtils,
   Firefly.Date;

   { TTogglTrackAPIv9 }

function TTogglTrackAPIv9.CreateTransport: TLayeredTransport;
begin
   Result := TLayeredTransport.CreateInstance;
   if Length(Self.FProxyHost) > 0 then begin
      Result.Proxy.Host := Self.FProxyHost;
      Result.Proxy.Port := IntToStr(Self.FProxyPort);
   end;
   Result.UserAgent := Self.UserAgent;
   Result.ContentType := 'application/json';
   case Self.AuthMethod of
      ttamToken: begin
         Result.Headers.Add('Authorization=Basic ' + EncodeBase64(Self.Token + ':' + 'api_token'));
      end;
      ttamLogin: begin
         Result.Headers.Add('Authorization=Basic ' + EncodeBase64(Self.LoginUsername + ':' + Self.LoginPassword));
      end;
   end;
end;

function TTogglTrackAPIv9.ExecuteGET(AnURL: string; out AnObject: TLayeredJSONObject): boolean;
var
   transport: TLayeredTransport;
begin
   AnObject := TLayeredJSONObject.CreateInstance(False);
   transport := CreateTransport;
   try
      Result := transport.GET(AnURL, AnObject);
      if not Result then begin
         FreeAndNil(AnObject);
      end;
   finally
      transport.Free;
   end;
end;

function TTogglTrackAPIv9.ExecutePOST(AnURL: string; AnInput: TLayeredJSONObject; out AnObject: TLayeredJSONObject): boolean;
var
   transport: TLayeredTransport;
begin
   AnObject := TLayeredJSONObject.CreateInstance(False);
   transport := CreateTransport;
   try
      Result := transport.POST(AnURL, AnInput, AnObject);
      if not Result then begin
         FreeAndNil(AnObject);
      end;
   finally
      transport.Free;
   end;
end;

function TTogglTrackAPIv9.ExecutePATCH(AnURL: string): boolean;
var
   transport: TLayeredTransport;
begin
   transport := CreateTransport;
   try
      Result := transport.PATCH(AnURL);
   finally
      transport.Free;
   end;
end;

function TTogglTrackAPIv9.GetProjects: TTogglTrackProjects;
begin
   if FProjects.Count = 0 then begin
      RefreshMe;
   end;
   Result := FProjects;
end;

function TTogglTrackAPIv9.GetTimeEntries: TTogglTrackTimeEntries;
begin
   if FTimeEntries.Count = 0 then begin
      RefreshMe;
   end;
   Result := FTimeEntries;
end;

constructor TTogglTrackAPIv9.Create;
begin
   FAuthMethod := ttamToken;
   FUserAgent := 'FireflyTogglTrackAPI/0.1';
   FTags := TTogglTrackTags.Create;
   FTags.FOwner := Self;
   FWorkspaces := TTogglTrackWorkspaces.Create;
   FWorkspaces.FOwner := Self;
   FProjects := TTogglTrackProjects.Create;
   FProjects.FOwner := Self;
   FClients := TTogglTrackClients.Create;
   FClients.FOwner := Self;
   FTimeEntries := TTogglTrackTimeEntries.Create;
   FTimeEntries.FOwner := Self;
end;

destructor TTogglTrackAPIv9.Destroy;
begin
   FProjects.Free;
   FTags.Free;
   FWorkspaces.Free;
   FClients.Free;
   FTimeEntries.Free;
   inherited Destroy;
end;

function TTogglTrackAPIv9.HasToken: boolean;
begin
   case Self.AuthMethod of
      ttamToken: begin
         Result := Length(FToken) > 0;
      end;
      ttamLogin: begin
         Result := (Length(FLoginUserName) > 0) and (Length(FLoginPassword) > 0);
      end;
   end;
end;

function TTogglTrackAPIv9.RefreshMe: boolean;
var
   json: TLayeredJSONObject;
   oTags: TLayeredJSONArray;
   oWorkspaces: TLayeredJSONArray;
   oProjects: TLayeredJSONArray;
   oClients: TLayeredJSONArray;
   oTimeEntries: TLayeredJSONArray;
begin
   Result := Self.ExecuteGET('https://api.track.toggl.com/api/v9/me?with_related_data=1', json);
   if Result then begin
      try
         if json.FindArray('tags', oTags) then begin
            FTags.LoadFromJSONArray(oTags);
         end;
         if json.FindArray('workspaces', oWorkspaces) then begin
            FWorkspaces.LoadFromJSONArray(oWorkspaces);
         end;
         if json.FindArray('projects', oProjects) then begin
            FProjects.LoadFromJSONArray(oProjects);
         end;
         if json.FindArray('clients', oClients) then begin
            FClients.LoadFromJSONArray(oClients);
         end;
         if json.FindArray('time_entries', oTimeEntries) then begin
            FTimeEntries.LoadFromJSONArray(oTimeEntries);
         end;
      finally
         json.Free;
      end;
   end;
end;

{
  StartTimer starts a new time entry.

  @seeAlso(https://developers.track.toggl.com/docs/api/time_entries#post-timeentries)
  @seeAlso(https://todoist.com/showTask?id=7108021888 Implement start call)
}
function TTogglTrackAPIv9.StartTimer(const AnEntry: TTogglTrackTimeCurrentEntry; AStartTime: TDateTime; AWorkspaceID: integer; ADescription: string;
   AProjectID: integer; ABillable: boolean): boolean;
var
   jsonInputBody: TLayeredJSONObject;
   jsonOutput: TLayeredJSONObject;
   sURL: string;
   sStart: string;
   dtStart: TDateTime;
begin
   sURL := 'https://api.track.toggl.com/api/v9/workspaces/' + IntToStr(AWorkspaceID) + '/time_entries';
   jsonInputBody := TLayeredJSONObject.CreateInstance(True);
   try
      dtStart := AStartTime + (GetLocalTimeOffset / 60 / 24);
      sStart := FormatDateTime('yyyy-mm-dd', dtStart) + 'T' + FormatDateTime('hh:nn:ss', dtStart) + 'Z';
      jsonInputBody.AddString('start', sStart);
      jsonInputBody.AddBoolean('billable', ABillable);
      jsonInputBody.AddString('created_with', FUserAgent);
      jsonInputBody.AddInt64('workspace_id', AWorkspaceID);
      jsonInputBody.AddInt64('duration', -1);
      if AProjectID > -1 then begin
         jsonInputBody.AddInt64('project_id', AProjectID);
      end;
      if Length(ADescription) > 0 then begin
         jsonInputBody.AddString('description', ADescription);
      end;
      jsonOutput := TLayeredJSONObject.CreateInstance(False);
      Result := Self.ExecutePOST(sURL, jsonInputBody, jsonOutput);
      if Result then begin
         try
            if Assigned(AnEntry) then begin
               AnEntry.LoadFromJSONObject(jsonOutput);
            end;
         finally
            jsonOutput.Free;
         end;
      end;
   finally
      jsonInputBody.Free;
   end;
end;

function TTogglTrackAPIv9.GetTimer(const AnEntry: TTogglTrackTimeCurrentEntry): boolean;
var
   json: TLayeredJSONObject;
begin
   try
      Result := Self.ExecuteGET('https://api.track.toggl.com/api/v9/me/time_entries/current', json);
      if Result then begin
         try
            AnEntry.LoadFromJSONObject(json);
         finally
            json.Free;
         end;
      end;
   except
      AnEntry.ID := -1;
   end;
end;

{
  StartTimer starts a new time entry.

  @seeAlso(https://developers.track.toggl.com/docs/api/time_entries#patch-stop-timeentry)
  @seeAlso(https://todoist.com/showTask?id=7108021715 Implement stop call)
}
function TTogglTrackAPIv9.StopTimer(AWorkspaceID, ATimeEntryID: int64): boolean;
var
   sURL: string;
begin
   sURL := 'https://api.track.toggl.com/api/v9/workspaces/' + IntToStr(AWorkspaceID) + '/time_entries/' + IntToStr(ATimeEntryID) + '/stop';
   Result := Self.ExecutePATCH(sURL);
end;

{ TTogglTrackTag }

procedure TTogglTrackTag.LoadFromJSONObject(AnObject: TLayeredJSONObject);
begin
   AnObject.FindInt64('id', Self.FID);
   AnObject.FindInt64('workspace_id', Self.FWorkspaceID);
   AnObject.FindString('name', Self.FName);
   AnObject.FindString('at', Self.FAt);
   // string deleted_at
end;

function TTogglTrackTag.DisplayText: string;
begin
   Result := FName;
end;

{ TTogglTrackProject }

procedure TTogglTrackProject.LoadFromJSONObject(AnObject: TLayeredJSONObject);
begin
   AnObject.FindInt64('id', Self.FID);
   AnObject.FindInt64('workspace_id', Self.FWorkspaceID);
   AnObject.FindInt64('client_id', Self.FClientID);
   AnObject.FindString('name', Self.FName);
   AnObject.FindBoolean('is_private', Self.FIsPrivate);
   AnObject.FindBoolean('active', Self.FActive);
   AnObject.FindString('at', Self.FAt);
   AnObject.FindString('created_at', Self.FCreatedAt);
   {
      "id": 151885496,                               // implemented
      "workspace_id": 3456635,                       // implemented
      "client_id": 44416210,                         // implemented
      "name": "Bose Headphone Alpha Test",           // implemented
      "is_private": false,                           // implemented
      "active": false,                               // implemented
      "at": "2023-01-30T17:03:47+00:00",             // implemented
      "created_at": "2019-05-29T06:44:53+00:00",     // implemented
      "server_deleted_at": null,
      "color": "#990099",
      "billable": null,
      "template": null,
      "auto_estimates": null,
      "estimated_hours": null,
      "rate": null,
      "rate_last_updated": null,
      "currency": null,
      "recurring": false,
      "recurring_parameters": null,
      "current_period": null,
      "fixed_fee": null,
      "actual_hours": 1,
      "wid": 3456635,
      "cid": 44416210
   }
end;

function TTogglTrackProject.DisplayText: string;
begin
   Result := FName;
end;

{ TTogglTrackClient }

procedure TTogglTrackClient.LoadFromJSONObject(AnObject: TLayeredJSONObject);
begin
   AnObject.FindInt64('id', Self.FID);
   AnObject.FindInt64('wid', Self.FWorkspaceID);
   AnObject.FindBoolean('archived', Self.FArchived);
   AnObject.FindString('name', Self.FName);
   AnObject.FindString('at', Self.FAt);
end;

function TTogglTrackClient.DisplayText: string;
begin
   Result := FName;
end;

{ TTogglTrackTimeEntry }

function TTogglTrackTimeEntry.GetStart: TDateTime;
begin
   try
      Result := ISO8601ToDate(FStartText);
   except
      Result := 0;
   end;
end;

function TTogglTrackTimeEntry.GetStop: TDateTime;
begin
   try
      Result := ISO8601ToDate(FStopText);
   except
      Result := 0;
   end;
end;

procedure TTogglTrackTimeEntry.LoadFromJSONObject(AnObject: TLayeredJSONObject);
begin
   AnObject.FindBoolean('billable', Self.FBillable);
   AnObject.FindString('description', Self.FDescription);
   AnObject.FindString('start', Self.FStartText);
   AnObject.FindString('stop', Self.FStopText);
   AnObject.FindInt64('id', Self.FID);
   AnObject.FindInt64('task_wid', Self.FTaskID);
   AnObject.FindInt64('workspace_id', Self.FWorkspaceID);
   AnObject.FindInt64('duration', Self.FDuration);
end;

function TTogglTrackTimeEntry.DisplayText: string;
begin
   Result := Self.FDescription;
end;

{ TTogglTrackTimeCurrentEntry }

function TTogglTrackTimeCurrentEntry.GetCurrentDuration: TDateTime;
begin
   Result := Now - ISO8601ToDate(FStartText, False);
end;

procedure TTogglTrackTimeCurrentEntry.LoadFromJSONObject(AnObject: TLayeredJSONObject);
begin
   inherited LoadFromJSONObject(AnObject);
   AnObject.FindString('at', Self.FAt);
end;

{ TTogglTrackDataObjects }

procedure TTogglTrackDataObjects<T>.ForEachObject(TheElementName: string; TheElement: TLayeredJSONObject; AnUserData: pointer; var Continue: boolean);
var
   Data: T;
begin
   Data := T.Create;
   if (Data is TTogglTrackDataObject) then begin
      TTogglTrackDataObject(Data).LoadFromJSONObject(TheElement);
      TTogglTrackDataObject(Data).FOwner := Self;
   end;
   Self.Add(Data);
   Continue := True;
end;

procedure TTogglTrackDataObjects<T>.LoadFromJSONArray(AnArray: TLayeredJSONArray);
begin
   Self.Clear;
   AnArray.ForEachObject(ForEachObject, nil);
end;

{ TTogglTrackWorkspace }

procedure TTogglTrackWorkspace.LoadFromJSONObject(AnObject: TLayeredJSONObject);
begin
   AnObject.FindInt64('id', Self.FID);
   AnObject.FindInt64('organization_id', Self.FOrganizationID);
   AnObject.FindString('name', Self.FName);

   AnObject.FindBoolean('premium', Self.FPremium);
   AnObject.FindBoolean('business_ws', Self.FBusinessWS);
   AnObject.FindBoolean('admin', Self.FAdmin);

   AnObject.FindBoolean('only_admins_may_create_projects', Self.FOnlyAdminsMayCreateProjects);
   AnObject.FindBoolean('only_admins_may_create_tags', Self.FOnlyAdminsMayCreateTags);
   AnObject.FindBoolean('only_admins_see_billable_rates', Self.FOnlyAdminsSeeBillableRates);
   AnObject.FindBoolean('only_admins_see_team_dashboard', Self.FOnlyAdminsSeeTeamDashboard);
   AnObject.FindBoolean('projects_billable_by_default', Self.FProjectsBillableByDefault);
   AnObject.FindBoolean('reports_collapse', Self.FReportsCollapse);

   AnObject.FindString('api_token', Self.FAPIToken);
   AnObject.FindString('at', Self.FAt);
   {
    "id": number,                    // implemented
    "organization_id": number,       // implemented
    "name": "string",                // implemented
    "profile": number,
    "premium": false,                 // implemented
    "business_ws": false,             // implemented
    "admin": true,                    // implemented
    "suspended_at": null,
    "server_deleted_at": null,
    "default_hourly_rate": null,
    "rate_last_updated": null,
    "default_currency": "USD",

    "only_admins_may_create_projects": false,
    "only_admins_may_create_tags": false,
    "only_admins_see_billable_rates": false,
    "only_admins_see_team_dashboard": false,
    "projects_billable_by_default": true,
    "reports_collapse": true,

    "rounding": number,
    "rounding_minutes": number,
    "api_token": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",                    // implemented
    "at": "2019-05-28T17:58:54+00:00",                                  // implemented
    "logo_url": "https://assets.track.toggl.com/images/workspace.jpg",
    "ical_url": "/ical/workspace_user/hexidstring",
    "ical_enabled": true,
    "csv_upload": null,
    "subscription": null,
    "working_hours_in_minutes": null
 }
end;

function TTogglTrackWorkspace.DisplayText: string;
begin
   Result := FName;
end;

end.
