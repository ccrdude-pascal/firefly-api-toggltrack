# Contributing to the FreePascal Toggl Track API

You need to use an up to date (currently trunk) version of FreePascal (3.3.1)
to support custom attributes, which are used e.g. for the display of objects.

You need to keep the formatting, e.g. by using the jcfsettings.cfg included
in this repository.

You can add missing attributes by following the given style.

You can add missing data groups by following the given style.

Add your changes to the changelog at each files header.

For any actions or other stuff that does not have an existing way to reuse,
use the Issues tracker on the repository to request a feature and express
your wish to help.

If you don't like the way this is implemented, feel free to clone the repository
or copy parts of interest.
