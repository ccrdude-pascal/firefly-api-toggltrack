{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit FireflyAPITogglTrack;

{$warn 5023 off : no warning about unused units}
interface

uses
  Firefly.TogglTrack.V9, Firefly.TogglTrack.DialogProjectPicker, 
  LazarusPackageIntf;

implementation

procedure Register;
begin
end;

initialization
  RegisterPackage('FireflyAPITogglTrack', @Register);
end.
