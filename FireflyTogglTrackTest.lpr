program FireflyTogglTrackTest;

{$mode objfpc}{$H+}

uses
   {$IFDEF UNIX}
   cthreads,
   {$ENDIF}
   {$IFDEF HASAMIGA}
   athreads,
   {$ENDIF}
   Interfaces, // this includes the LCL widgetset
   Forms,
   FireflyTogglTrackTest.FormMain,
   Layers.JSON.FreePascal,
   Layers.Transport.Synapse;

   {$R *.res}

begin
   RequireDerivedFormResource := True;
  Application.Scaled:=True;
   Application.Initialize;
   Application.CreateForm(TFormFireflyTogglTrackTest, FormFireflyTogglTrackTest);
   Application.Run;
end.
