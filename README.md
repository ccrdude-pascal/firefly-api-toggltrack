# Firefly TogglTrack

This is a FreePascal interface to
[version 9 of the Toggl Track API](https://developers.track.toggl.com/).

Toggl Track is a service that allows you to track time for projects you work on.

## Dependencies

This package uses the following other packages:
* FreePascal version of at least 3.3.1 because custom attributes are used.
* [Firefly Core](https://gitlab.com/ccrdude-pascal/firefly-core)
* [Firefly Layers](https://gitlab.com/ccrdude-pascal/firefly-layers) for
  abstract layers to acess the Internet and work with JSON files, allowing to
  extend the library to e.g. Delphi, where other libraries for that purpose
  might be available.
* [Firefly Object Visualization Mapping](https://gitlab.com/ccrdude-pascal/firefly-ovm)
  to simplify display of API lists / objects.
